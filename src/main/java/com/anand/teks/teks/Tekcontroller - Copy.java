package com.anand.teks.teks;

import java.util.ArrayList;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.anand.model.User;
import com.anand.model.UserStatus;
import com.anand.service.TekService;

@RestController
@EnableAutoConfiguration
@ComponentScan("com.anand.*")
@EnableJpaRepositories("com.anand.*")
@EntityScan("com.anand.*")
public class Tekcontroller implements ErrorController {

	@Autowired
	private TekService tekService;

	@RequestMapping(ControllerConstant.USER_BY_ID)
	public ResponseEntity<Object> getUserDetailsbyId(@PathVariable("id") int id) {
		Optional<User> users = tekService.getUserById(id);
		return new ResponseEntity<Object>(users, HttpStatus.OK);
	}

	@RequestMapping(value = ControllerConstant.ADD_NEW_USER, method = RequestMethod.POST)
	public ResponseEntity<Boolean> addNewUser(@RequestBody User user) {
		UserStatus userStatus = new UserStatus();
		userStatus.setIsUserAdded(tekService.addUserNewToDB(user));
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}

	@RequestMapping(value = ControllerConstant.USER_BY_ID, method = RequestMethod.DELETE)
	public ResponseEntity<Object> deleteUser(@PathVariable("id") int id) {
		tekService.deleteUserById(id);
		return new ResponseEntity<Object>(true, HttpStatus.OK);
	}
	
	@RequestMapping(value = ControllerConstant.GET_USER_BY_LN, method = RequestMethod.GET)
	public ResponseEntity<Object> getUserDetailsByLName(@PathVariable("lName") String lName) {
		ArrayList<User> uList = tekService.getUserByLastName(lName);
		return new ResponseEntity<Object>(uList, HttpStatus.OK);
	}
	
	

	@RequestMapping(value = ControllerConstant.PATH)
	public String error() {
		return "Error handling";
	}

	@Override
	public String getErrorPath() {
		return ControllerConstant.PATH;
	}

}

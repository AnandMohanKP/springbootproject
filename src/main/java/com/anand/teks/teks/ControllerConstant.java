package com.anand.teks.teks;

public class ControllerConstant {
	
	public static final String USER_BY_ID = "/user/{id}";
	public static final String PATH = "/error";
	public static final String ADD_NEW_USER = "/user";
	public static final String GET_USER_BY_LN = "/userDetails/{lName}";
	
}

package com.anand.teks.teks;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeksApplication {
	public static void main(String[] args) {
		SpringApplication.run(TeksApplication.class, args);
	}
}


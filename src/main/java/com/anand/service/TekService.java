package com.anand.service;

import java.util.ArrayList;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.anand.model.User;
import com.anand.repo.PlayerRepository;

@Service
public class TekService {
	
	@Autowired
	private PlayerRepository playerRepository;
	
	public Optional<User> getUserById(Integer id){
		Optional<User> user = playerRepository.findById(id);
		return user;
	}
	
	public boolean addUserNewToDB(User user) {
		playerRepository.save(user);
		return true;
	}
	
	public boolean deleteUserById(int id) {
		playerRepository.deleteById(id);
		return true;
	}
		
	
	public ArrayList<User> getUserByLastName(String lName) {
		ArrayList<User> usersList = (ArrayList<User>) playerRepository.findByLastName(lName);
		return usersList;
	}
}

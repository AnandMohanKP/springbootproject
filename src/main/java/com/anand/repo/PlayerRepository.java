package com.anand.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.anand.model.User;

@Repository
public interface PlayerRepository extends CrudRepository<User, Integer> {

	public Optional<User> findById(Integer id);
	public void deleteById(Integer id);
	
	@Query("SELECT u FROM User u WHERE u.lastName = :lastName")
    public List<User> findByLastName(@Param("lastName") String lastName);
	

}
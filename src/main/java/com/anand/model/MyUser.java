package com.anand.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

@PropertySource(ignoreResourceNotFound=true,value="classpath:demo.properties")
public class MyUser {
	@Value("${test.firstName}")
	String firstName;
	@Value("${test.lastName}")
	String lastName;
	@Value("${test.email}")
	String email;
	@Value("${test.phoneNo}")
	String phoneNo;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
}

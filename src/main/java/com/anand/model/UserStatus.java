package com.anand.model;

public class UserStatus {
	private Boolean isUserAdded;

	public Boolean getIsUserAdded() {
		return isUserAdded;
	}

	public void setIsUserAdded(Boolean isUserAdded) {
		this.isUserAdded = isUserAdded;
	}
}
